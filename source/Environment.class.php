<?php


namespace DreamApplyTest;


use Dotenv\Dotenv;


class Environment
{
    private static function getSourcePath()
    {
        $path = dirname(__FILE__) . "/../";
        return ( file_exists($path) ? $path : false );
    }

    public static function setTimeZone()
    {
        $tz     = 'UTC';
        $source = self::getSourcePath();
        if( false !== $source ) {
            $env = new Dotenv($source);
            $env->load();
            $tz = getenv('TIMEZONE');
        }

        date_default_timezone_set($tz);
    }
}