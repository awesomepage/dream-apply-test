<?php


namespace DreamApplyTest;


use DreamApplyTest\Helpers\DateHelper;


class AcademicYear
{

    private $name;
    private $terms;

    public function __construct( $name )
    {
        $this->name  = $name;
        $this->terms = new AcademicTermCollection();
    }

    public function addTerm( $term )
    {
        if( ( $term instanceof AcademicTerm ) ) {
            $this->terms[$term->getName()] = $term;
        }
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function getAllTerms()
    {
        return $this->terms->getAll();
    }

    public function termsToStringArray()
    {
        return array_map(function ( $t ) {
            return (string)$t;
        }, $this->terms->getAll());
    }

    public function containsDate( $bean )
    {
        $date = DateHelper::parseDate($bean);
        foreach( $this->getAllTerms() as $term ){
            if( in_array( $bean->format('Ymd'), $term->getDateRangeAsArray('Ymd') ) ){
                return true;
            } 
        }
        
        if( count($this->getAllTerms()) > 1 ){
            $termNames = array_keys($this->getAllTerms());
            for( $i = 1; $i < count($this->getAllTerms()); $i++ ){
                $prevTerm = $this->terms[$termNames[$i-1]];
                $prevEndDate = $prevTerm->getEndDateTimestamp();
                $nextTerm = $this->terms[$termNames[$i]];
                $nextStartDate = $nextTerm->getStartDateTimestamp();
                $range = DateHelper::getDatePeriodAsArray( $prevEndDate, $nextStartDate, 'Ymd' );
                if( in_array( $date->format('Ymd'), $range ) ){
                    return true;
                }
            }
            
        }
        
        return false;
    }

}