<?php


namespace DreamApplyTest;


abstract class BaseCollection implements \ArrayAccess
{
    protected $stack = array();

    public function offsetExists( $offset )
    {
        return array_key_exists($offset, $this->stack);
    }

    public function offsetGet( $offset )
    {
        return $this->stack[$offset];
    }

    public function offsetUnset( $offset )
    {
        unset( $this->stack[$offset] );
    }

    public function getAll()
    {
        return $this->stack;
    }
}