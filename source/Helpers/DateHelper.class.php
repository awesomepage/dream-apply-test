<?php


namespace DreamApplyTest\Helpers;


use DatePeriod;
use DateInterval;
use Carbon\Carbon;


class DateHelper
{
    public static function parseDate( $bean )
    {
        $date = null;
        if( is_int($bean) ) {
            $date = Carbon::createFromTimestamp($bean);
        } elseif( is_string($bean) ) {
            $date = Carbon::createFromFormat('Y-m-d', $bean);
        } elseif( $bean instanceof \DateTime ) {
            $date = Carbon::createFromDate($bean->format('Y'), $bean->format('n'), $bean->format('j'));
        }

        return $date;
    }

    public static function getDatePeriod( $startDate, $endDate )
    {
        if( is_int($startDate) || is_string($startDate) ){
            $startDate = self::parseDate($startDate);
        }
        if( is_int($endDate) || is_string($endDate) ){
            $endDate = self::parseDate($endDate);
        }

        $interval = new DateInterval('P1D'); //1 Day
        $period   = new DatePeriod($startDate, $interval, $endDate);
        
        return $period;
    }

    public static function getDatePeriodAsArray( $startDate, $endDate, $format )
    {
        $period = self::getDatePeriod($startDate,$endDate);

        $range    = array();
        foreach( $period as $d ) {
            $range[] = $d->format($format);
        }
        return $range;
    }
}