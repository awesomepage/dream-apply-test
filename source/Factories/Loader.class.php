<?php


namespace DreamApplyTest\Factories;


use DreamApplyTest\AcademicTerm;
use DreamApplyTest\AcademicYear;
use DreamApplyTest\AcademicYearCollection;
use Exception;
use Symfony\Component\Yaml\Parser;
use DreamApplyTest\Environment;


class Loader
{
    private $filePath;
    private $yamlData;

    public function __construct( $filePath )
    {
        Environment::setTimeZone();

        if( empty( $filePath ) || !file_exists($filePath) ) {
            throw new Exception('Data Source File Not Found or Path Empty!');
        }

        $this->filePath = $filePath;

        $this->loadYAML();

    }

    private function loadYAML()
    {
        if( file_exists($this->filePath) ) {
            $yaml           = new Parser();
            $this->yamlData = $yaml->parse(file_get_contents($this->filePath));
        }

        return $this;
    }

    public function getRawData()
    {
        return $this->yamlData;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public static function loadData( $filePath )
    {
        $me            = new self($filePath);
        $academicYears = new AcademicYearCollection();

        foreach( $me->getRawData() as $yearName => $terms ) {
            $year = new AcademicYear($yearName);
            foreach( $terms as $termName => $dates ) {
                $term = new AcademicTerm($termName, $dates[0], $dates[1]);
                $year->addTerm($term);
            }
            $academicYears->addYear($year);
        }

        return $academicYears;
    }

}