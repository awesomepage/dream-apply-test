<?php


namespace DreamApplyTest\Tests;


use PHPUnit_Framework_TestCase;
use DreamApplyTest\AcademicTerm;


class AcademicTermTest extends PHPUnit_Framework_TestCase
{
    public function testException()
    {
        $this->setExpectedException('InvalidArgumentException', 'Data missing');
        $t1 = new AcademicTerm('Term Test', '1', '1');
    }
    
    public function testIsValid()
    {
        // Test Invalid Integers
        $t1 = new AcademicTerm('Term 1', 1, 1);
        $this->assertEquals('Invalid Academic Term!', (string)$t1);
        $this->assertTrue(!$t1->isValid());

        // Test Valid integers
        $t1v = new AcademicTerm('Term 1', 1409529600, 1418169600);
        $expectedString = "Academic Term: Term 1. Start Date: 2014-09-01. End Date: 2014-12-10. Days: 100";
        $this->assertEquals($expectedString, (string)$t1v);
        $this->assertTrue($t1v->isValid());
        // Assert Date format
        $this->assertEquals('2014-09-01',$t1v->getStartDate());

        // Test String
        $t2 = new AcademicTerm('Term 2', '2014-09-01', '2014-12-20');
        $expectedString = "Academic Term: Term 2. Start Date: 2014-09-01. End Date: 2014-12-20. Days: 110";
        $this->assertEquals($expectedString, (string)$t2);
        $this->assertTrue($t2->isValid());
        // Assert Date format
        $this->assertEquals('2014-09-01',$t2->getStartDate());

        // Test DateTime format
        $t3 = new AcademicTerm('Term 3', \DateTime::createFromFormat('Y-m-d', '2015-01-01'), '2015-04-20');
        $expectedString = "Academic Term: Term 3. Start Date: 2015-01-01. End Date: 2015-04-20. Days: 109";
        $this->assertEquals($expectedString, (string)$t3);
        $this->assertTrue($t3->isValid());
        
        // Test DateRange
        $this->assertTrue( is_array( $t3->getDateRangeAsArray('Ymd') ) );
        // Verify date exists
        $this->assertTrue( in_array( '20150101', $t3->getDateRangeAsArray('Ymd') ) );
        $this->assertTrue( in_array( '20150108', $t3->getDateRangeAsArray('Ymd') ) );
        $this->assertTrue( in_array( '20150110', $t3->getDateRangeAsArray('Ymd') ) );
    }
}