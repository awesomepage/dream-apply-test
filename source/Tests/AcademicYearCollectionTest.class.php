<?php
/**
 * Created by PhpStorm.
 * User: pagecarbajal
 * Date: 4/24/16
 * Time: 11:31 AM
 */

namespace DreamApplyTest\Tests;


use PHPUnit_Framework_TestCase;
use DreamApplyTest\Factories\Loader;

class AcademicYearCollectionTest extends PHPUnit_Framework_TestCase
{

    public function testLoadData()
    {
        $filePath = dirname(__FILE__)."/../../data.yaml";
        $collection = Loader::loadData($filePath);
        // Collection is an object
        $this->assertTrue(is_object($collection));
        // Collection has 2014/15 hey
        $this->assertArrayHasKey('2014/15',$collection);
        // Academic Year 2014/15 has Autumn Semester key
        $this->assertArrayHasKey( 'Autumn Semester', $collection['2014/15']->getAllTerms() );
        
        // Real Tests
        // Get Term name from Date
        $this->assertEquals( 'Autumn Semester 2014/15', $collection->getTermNameFromDate('2014-10-01') );
        $this->assertEquals( 'Autumn Semester 2015/16', $collection->getTermNameFromDate('2016-01-01') );
        $this->assertEquals( 'Spring Semester 2015/16', $collection->getTermNameFromDate('2016-01-05') );
        $this->assertEquals( 'Second Trimester 2016/17', $collection->getTermNameFromDate('2016-12-20') );
        
    }
    
}