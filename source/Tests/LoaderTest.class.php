<?php
namespace DreamApplyTest\Tests;


use DreamApplyTest\Factories\Loader;
use PHPUnit_Framework_TestCase;


class LoaderTest extends PHPUnit_Framework_TestCase
{

    public function testException()
    {
        $this->setExpectedException('Exception', 'Data Source File Not Found or Path Empty!');
        new Loader(null);
    }

    public function testDataFileExists()
    {
        $filePath = dirname(__FILE__) . "/../../data.yaml";
        $this->assertTrue(file_exists($filePath));
    }

    public function testDataParsing()
    {
        $filePath = dirname(__FILE__) . "/../../data.yaml";
        $loader   = new Loader($filePath);

        // Date has 3 periods
        $this->assertEquals(3, count($loader->getRawData()));
        // Data has a key for 2014/15
        $this->assertArrayHasKey('2014/15', $loader->getRawData());
        // Data has a key for 2015/16
        $this->assertArrayHasKey('2015/16', $loader->getRawData());
        // Data has a key for 2016/17
        $this->assertArrayHasKey('2016/17', $loader->getRawData());

    }

}