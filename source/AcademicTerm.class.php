<?php

namespace DreamApplyTest;


use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use DreamApplyTest\Helpers\DateHelper;


class AcademicTerm
{
    private $name;
    private $startDate;
    private $endDate;
    private $academicYear;
    private $isValid;

    public function __construct( $name, $startDate, $endDate )
    {
        Environment::setTimeZone();

        $this->name      = $name;
        $this->startDate = DateHelper::parseDate($startDate);
        $this->endDate   = DateHelper::parseDate($endDate);
        $this->isValid   = true;

        if( empty( $name ) || null == $this->startDate || null == $this->endDate ) {
            $this->isValid = false;
        }

        if( $this->getCalendarDays() < 1 ) {
            $this->isValid = false;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCalendarDays()
    {
        if( $this->isValid ) {
            if( $this->startDate instanceof Carbon ) {
                return $this->startDate->diffInDays($this->endDate);
            }
        }

        return 0;
    }

    public function getStartDate( $format = 'Y-m-d' )
    {
        return $this->startDate->format($format);
    }

    public function getStartDateTimestamp()
    {
        return $this->startDate->timestamp;
    }

    public function getStartDateObject()
    {
        return $this->startDate;
    }

    public function getEndDate( $format = 'Y-m-d' )
    {
        return $this->endDate->format($format);
    }

    public function getEndDateTimestamp()
    {
        return $this->endDate->timestamp;
    }

    public function getEndDateObject()
    {
        return $this->endDate;
    }

    public function getDateRange()
    {
        return DateHelper::getDatePeriod($this->startDate, $this->endDate);
    }

    public function getDateRangeAsArray( $format = 'Y-m-d' )
    {
        return DateHelper::getDatePeriodAsArray($this->startDate, $this->endDate, $format);
    }

    public function isValid()
    {
        return $this->isValid;
    }

    public function __toString()
    {
        $str = 'Invalid Academic Term!';
        if( $this->isValid ) {
            $sd   = $this->getStartDate();
            $ed   = $this->getEndDate();
            $days = $this->getCalendarDays();
            $str  = "Academic Term: {$this->name}. Start Date: {$sd}. End Date: {$ed}. Days: {$days}";
        }
        return $str;
    }

}