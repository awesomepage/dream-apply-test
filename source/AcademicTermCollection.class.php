<?php


namespace DreamApplyTest;


use DreamApplyTest\BaseCollection;


final class AcademicTermCollection extends BaseCollection
{
    public function __construct()
    {
        $this->stack = array();
    }

    public function offsetSet( $offset, $term )
    {
        if( $term instanceof AcademicTerm && $term->isValid() && $this->validTermDates($term) ) {
            $this->stack[$term->getName()] = $term;
        }
    }

    private function validTermDates( AcademicTerm $term )
    {
        foreach( $this->stack as $currentTerm ) {
            if( $currentTerm->getName() == $term->getName() ) {
                continue;
            }
            if( in_array($term->getStartDate('Ymd'), $currentTerm->getDateRangeAsArray('Ymd')) || in_array($term->getEndDate('Ymd'), $currentTerm->getDateRangeAsArray('Ymd')) ) {
                throw new \Exception('Term Date Overlap!');
            }
        }

        return true;
    }

}