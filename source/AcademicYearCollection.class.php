<?php


namespace DreamApplyTest;


use DreamApplyTest\Helpers\DateHelper;


final class AcademicYearCollection extends BaseCollection
{
    public function construct()
    {
        $this->stack = array();
    }

    public function addYear( $bean )
    {
        if( $bean instanceof AcademicYear ) {
            $this->stack[$bean->getName()] = $bean;
        }
    }

    public function offsetSet( $offset, $value )
    {
        if( $value instanceof AcademicYear ) {
            $this->stack[$offset] = $value;
        }
    }

    public function getTermNameFromDate( $bean )
    {
        //$date = DateHelper::parseDate($bean);

        $term = $this->getTermFromDate($bean);
        if( null !== $term ){
            $termName = $term->getName();
            $yearName = $this->getAcademicYearFromDate($bean);
            return "{$termName} {$yearName}";
        }
        
        return null;
    }

    public function getTermFromDate( $bean )
    {
        $date = DateHelper::parseDate($bean);

        foreach( $this->stack as $year ) {
            if( $year instanceof AcademicYear ) {
                foreach( $year->getAllTerms() as $name => $term ) {
                    if( in_array($date->format('Ymd'), $term->getDateRangeAsArray('Ymd')) ) {
                        return $term;
                    }
                }
                // A date not in the middle of the terms
                if( $year->containsDate($date) ) {
                    $termNames = array_keys($year->getAllTerms());
                    $terms = $year->getAllTerms();
                    if( count($termNames) > 1 ) {
                        for( $i = 1; $i < count($termNames); $i++ ) {
                            $prevTerm = $terms[$termNames[$i - 1]];
                            $nextTerm = $terms[$termNames[$i]];
                            $range    = DateHelper::getDatePeriodAsArray($prevTerm->getEndDate(), $nextTerm->getStartDate(), 'Ymd');
                            if( in_array($date->format('Ymd'), $range) ) {
                                return $terms[$termNames[$i - 1]];
                            }
                        }
                    } else {
                        return $terms[$termNames[0]];
                    }
                }
            }
        }

        return null;
    }    
    
    public function getAcademicYearFromDate( $date )
    {
        $date = DateHelper::parseDate($date);

        foreach( $this->stack as $name => $academicYear ) {
            if( $academicYear->containsDate($date) ) {
                return $name;
            }
        }

        return null;
    }

}