<?php

namespace DreamApplyTest;


use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DreamApplyTest\Factories\Loader;

class CLI
{
    private static $output;
    
    public function __construct()
    {
        $app = new Application('DreamApplyTest', '1.0.0');
        $app->register('load')
            ->setDescription('CLI for code test')
            ->addArgument('file', InputArgument::REQUIRED, 'data format file path')
            ->addArgument('date', InputArgument::REQUIRED, 'Date to test')
            ->setCode(function ( InputInterface $input, OutputInterface $output ) {
                CLI::setOutPutInterface($output);
                CLI::renderMessage('Loading Data from: ' . $input->getArgument('file'));
                $filePath = $input->getArgument('file');
                $date = $input->getArgument('date');
                if( file_exists($filePath) ){
                    $academicYears = Loader::loadData($filePath);
                    
                    CLI::renderMessage( 'Total Academic Years Loaded: ' . count($academicYears->getAll()) );
                    CLI::renderMessage('-- Tests 1: ');
                    CLI::renderMessage('Get Academic Year name from date: ' . $date);
                    $yearName = $academicYears->getAcademicYearFromDate($date);
                    CLI::renderMessage( 'Academic Year: ' . $yearName );
                    CLI::renderMessage('-- Tests 2: ');
                    CLI::renderMessage('Get all academic terms for the Academic year: ' . $date);
                    CLI::renderMessage( 'Academic Year: ' . $yearName );
                    CLI::renderMessage('Has the following Terms:');
                    $year = $academicYears[$yearName];
                    foreach($year->termsToStringArray() as $term){
                        CLI::renderMessage(" • {$term}");
                    }
                    CLI::renderMessage('-- Tests 3: ');
                    CLI::renderMessage('Get Academic Term name from date: ' . $date);
                    $termName = $academicYears->getTermNameFromDate($date);
                    CLI::renderMessage("Term name: {$termName}");

                    CLI::renderMessage('-- Tests 4: ');
                    CLI::renderMessage('Get Academic Term calendar days from date: ' . $date);
                    $term = $academicYears->getTermFromDate($date);
                    CLI::renderMessage("Term calendar days: " . $term->getCalendarDays() );

                } else {
                    CLI::renderMessage("File not found!");
                }
            });
        $app->run();
    }

    private static function setOutPutInterface( $output )
    {
        self::$output = $output;
    }
    
    private static function renderMessage( $message)
    {
        self::$output->writeln("<info>{$message}</info>");
    }
}