#DreamApply.com Code Test

Build a small Command Line Interface to interact with the Academic Years and Academic Terms data models. 
 
The CLI will be invoked using two parameters. The path of the input file from where to load the information and a Date. 

##Considerations

1. There is a small bug on Symofny/yaml/inline so we are using the timezone = UTC


##Change Log


###TODO

1. Design a Data Format to represent the academic years and terms
    - Data Format has to be Human-Editable
    - Years and Terms can be configured differently along the file
    - Design the Data Format to be as elegant as possible
2. Design Classes for academic years and academic terms with methods to answer the questions in point 4
    - Academic terms cannot overlap dates.
3. Create a Factory to load the data and build years and terms objects
4. Implement methods for the following cases
    - Return the academic year object given a date
        - Decide what to do if the academic year is not configured
    - An academic year is in effect until the next academic year has started
        - Test for 2015-09-03
    - Given the academic year (AY), get it's name, e.g. "2015/16" 
    - Given the academic year (AY), return all the academic terms (AT) that belong to it
    - Given the academic term (AT), print it's name, e.g "Spring 2015/16" 
    - Given the academic term (AT), calculate it's length in calendar days
5. Write a Command Line Interface
    - The CLI takes two arguments path to file, and a date
    - The script should provide answers for all the questions above


###Version 1.0.0

- Added app.php file
- Added CLI class
- Started Running tests for CLI
- Added AcademicYearCollectionTest
- 2. Added method AcademicYear/getAcademicYearFromDate
- 5. Added method AcademicYearCollection/getCurrentTermName -> getTermFromDate
- Added DateHelper class
- 4. Added method AcademicYear/getAllTerms
- 3. Added method AcademicYear/getName
- 6. Added method AcademicYear/getCurrentTermCalendarDays
- Implemented TermCollection offsetSet method
- Added method TermCollection/validTermDates
- Added methods AcademicTerm/getDateRange and getDateRangeAsArray 
- Added methods AcademicTerm/getStartDateObject and getEndDateObject 
- Added methods AcademicTerm/getStartDateTimestamp and getEndDateTimestamp 
- Added classes BaseCollection and TermCollection
- Added Environment class
- Added .env-demo to the repository
- Required phpdtoenv
- Added class AcademicTerm
- Added class AcademicYear
- Added Tests/LoaderTest class for some unit testing  
- Added empty class Factories/Loader
- Configured composer psr-4 autoload
- Required symfony YAML
- Created the YAML Data Format for Academic Years
- First commit